package com.shinys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoReactiveDataAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * "C:\Program Files\Java\jdk-11.0.3\bin\java.exe" -jar "C:\project\cqrs-eventsourcing\build\libs\cqrs-eventsourcing-0.0.1-SNAPSHOT.jar"
 *
 * @author shinys
 */
@SpringBootApplication(
		exclude = {
				MongoReactiveDataAutoConfiguration.class,
				DataSourceAutoConfiguration.class,
				DataSourceTransactionManagerAutoConfiguration.class
		}
)
@EntityScan(
		basePackageClasses = {
				Application.class,
				Jsr310JpaConverters.class
		}
)
@EnableAspectJAutoProxy(proxyTargetClass = false)
@EnableAsync
@EnableScheduling
@EnableReactiveMongoRepositories
@EnableKafka
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
