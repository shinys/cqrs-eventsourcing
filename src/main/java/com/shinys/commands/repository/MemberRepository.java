package com.shinys.commands.repository;

import com.shinys.commands.domain.MemberDomain;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 *  Command Handler 에서 처리하는 Domain Object Aggregate.
 *
 * @author shinys
 */
@Repository("memberCommandRepository")
public interface MemberRepository extends ReactiveMongoRepository<MemberDomain, Long> {
}
