package com.shinys.commands.repository;

import com.shinys.commands.domain.CardDomain;
import com.shinys.queries.repository.entity.Card;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 *
 * @author shinys
 */
@Repository("cardCommandRepository")
public interface CardRepository extends ReactiveMongoRepository<CardDomain, Long> {

    Flux<CardDomain> findByCardNoIn(List<String> cardNos, Sort sort);

    Mono<CardDomain> findFirstByCardNo(String cardNo);
}
