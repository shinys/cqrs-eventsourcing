package com.shinys.commands.domain;

import com.fasterxml.jackson.annotation.*;
import com.shinys.queries.repository.entity.Member;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author shinys
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@Document(collection = "cardDomain")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CardDomain implements Serializable {
    private static final long serialVersionUID = 6512332286534302515L;
    @Id
    @JsonIgnore
    private ObjectId id;

    private String cardNo;

    private Long communityNo;

    private long memberNo;

    protected String title;

    protected String content;

    @CreatedDate
    protected LocalDateTime regDt;

    @LastModifiedDate
    protected LocalDateTime updDt;
}
