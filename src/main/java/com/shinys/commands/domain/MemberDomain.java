package com.shinys.commands.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 *
 * @author shinys
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@Document(collection = "memberDomain")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberDomain implements Serializable {
    private static final long serialVersionUID = 6512332286534302515L;

    @Id
    @JsonIgnore
    private ObjectId id;

    private long memberNo;

    private String nick;
}
