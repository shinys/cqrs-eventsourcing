package com.shinys.commands.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shinys.commands.domain.CardDomain;
import com.shinys.commands.repository.CardRepository;
import com.shinys.queries.repository.entity.Card;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Objects;

/**
 * Command Handler 역할수행하는 Service.
 * 수신한 데이터 유효성을 검사하고 실제 비즈니스 로직을 처리하며 도메인 이벤트를 생성하고 이벤트 버스로 전파.
 *
 * @author shinys
 */
@Service
public class CardCommandService {
    private static final Logger logger = LogManager.getLogger(CardCommandService.class);

    private KafkaTemplate<String,String> kafkaTemplate;

    private ObjectMapper mapper;

    private CardRepository cardRepository;

    @Autowired
    public CardCommandService(
            @Qualifier("cardCommandRepository") CardRepository cardRepository,
            KafkaTemplate<String,String> kafkaTemplate,
            ObjectMapper mapper
    ){
        this.cardRepository = cardRepository;
        this.kafkaTemplate = kafkaTemplate;
        this.mapper = mapper;
    }

    public Mono<CardDomain> save(CardDomain create){
        //입력검증 , aggregate 저장.
        return cardRepository.findFirstByCardNo(create.getCardNo())
                .defaultIfEmpty(new CardDomain())
                .map(savedDomain->{
                    logger.info("Card entity : {}",savedDomain::toString);
                    if(Objects.nonNull(savedDomain.getId())){
                        throw new DuplicateKeyException(String.format("duplicated cardNo:%s",savedDomain.getCardNo()));
                    }else{
                        Mono<CardDomain> saved = cardRepository.save(create);

                        //이벤트 전송
                        try {
                            //이벤트객체 생성 후 event bus에 송신
                            //여기서는 String을 이벤트객체로 사용하지만, 각 이벤트별 타입을 지정하여 type-safe하게 처리.
                            String eventPayload = mapper.writer().writeValueAsString(create);
                            logger.info("Send event to EventBus topic:{}, payload:{}",()->"card.create",()-> eventPayload);
                            kafkaTemplate.send("card.create", eventPayload);
                        } catch (JsonProcessingException e) {
                            logger.error(create::toString,e);
                        }
                        return saved.block();
                    }
                });

    }


    public Mono<CardDomain> update(CardDomain update){
        return cardRepository.findFirstByCardNo(update.getCardNo())
                .defaultIfEmpty(new CardDomain())
                .map( savedDomain -> {
                    if( Objects.isNull(savedDomain.getId()) ){
                        throw new IllegalStateException("Data Not Exists. cardNo:"+update.getCardNo());
                    }else{
                        //savedDomain , update merge
                        update.setId(savedDomain.getId());
                        Mono<CardDomain> updated = cardRepository.save(update);

                        //이벤트 전송
                        try {
                            String eventPayload = mapper.writer().writeValueAsString(update);
                            logger.info("Send event to EventBus topic:{}, payload:{}",()->"card.update",()-> eventPayload);
                            kafkaTemplate.send("card.update", eventPayload);
                        } catch (JsonProcessingException e) {
                            logger.error(update::toString,e);
                        }

                        return updated.block();
                    }
                });
    }

}
