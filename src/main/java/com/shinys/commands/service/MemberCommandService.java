package com.shinys.commands.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shinys.commands.domain.MemberDomain;
import com.shinys.commands.repository.MemberRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * @author shinys
 */
@Service
public class MemberCommandService {
    private static final Logger logger = LogManager.getLogger(MemberCommandService.class);

    private KafkaTemplate<String,String> kafkaTemplate;

    private ObjectMapper mapper;

    private MemberRepository memberRepository;

    @Autowired
    public MemberCommandService(
            @Qualifier("memberCommandRepository") MemberRepository memberRepository,
            KafkaTemplate<String,String> kafkaTemplate,
            ObjectMapper mapper
    ){
        this.memberRepository = memberRepository;
        this.kafkaTemplate = kafkaTemplate;
        this.mapper = mapper;
    }

    public Mono<MemberDomain> save(MemberDomain domain){
        return memberRepository.save(domain)
                .map(memberDomain -> {

                    //이벤트 전송
                    try {
                        String eventPayload = mapper.writer().writeValueAsString(memberDomain);
                        logger.info("Send event to EventBus topic:{}, payload:{}",()->"member.update",()-> eventPayload);
                        kafkaTemplate.send("member.update", eventPayload);
                    } catch (JsonProcessingException e) {
                        logger.error(memberDomain::toString,e);
                    }

                    return memberDomain;
                });
    }
}
