package com.shinys.commands.api;

import com.shinys.commands.domain.CardDomain;
import com.shinys.commands.domain.MemberDomain;
import com.shinys.commands.service.CardCommandService;
import com.shinys.commands.service.MemberCommandService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 명령 인터페이스
 * @author shinys
 */
@RestController
@RequestMapping("/api/command")
public class ManuplateController {
    private final static Logger logger = LogManager.getLogger(ManuplateController.class);

    private static final AtomicLong cardSequence = new AtomicLong(1L);
    private static final AtomicLong memberSequence = new AtomicLong(1L);

    private CardCommandService cardCommandService;

    private MemberCommandService memberCommandService;

    @Autowired
    public ManuplateController(
            CardCommandService cardCommandService,
            MemberCommandService memberCommandService
    ){
        this.cardCommandService = cardCommandService;
        this.memberCommandService = memberCommandService;
    }


    @PostMapping("/card")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<String> createCard(
            @RequestBody CardDomain cardDomain
    ){
        long cardNo = cardSequence.getAndIncrement();
        cardDomain.setCardNo( String.valueOf(cardNo) );
        //이 응답은 명령에대한 처리가 완료를 의미하며, 쿼리용 정규화뷰에 데이터가 반영된 것을 의미하지 않음.
        return cardCommandService.save(cardDomain)
                .map(c->c.getCardNo());
    }


    @PutMapping("/card")
    @ResponseStatus(HttpStatus.OK)
    public Mono<String> updateCard(
            @RequestBody CardDomain cardDomain
    ){
        //이 응답은 명령에대한 처리가 완료를 의미하며, 쿼리용 정규화뷰에 데이터가 반영된 것을 의미하지 않음.
        return cardCommandService.update(cardDomain)
                .map(c->c.getCardNo());
    }


    @PostMapping("/member")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Long> createCard(
            @RequestBody MemberDomain memberDomain
    ){
        long memberNo = memberSequence.getAndIncrement();
        memberDomain.setMemberNo(memberNo);
        //이 응답은 명령에대한 처리가 완료를 의미하며, 쿼리용 정규화뷰에 데이터가 반영된 것을 의미하지 않음.
        return memberCommandService.save(memberDomain)
                .map(m->m.getMemberNo());
    }

}
