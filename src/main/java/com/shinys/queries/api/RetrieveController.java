package com.shinys.queries.api;

import com.shinys.queries.api.dto.ResponseDto;
import com.shinys.queries.repository.MemberRepository;
import com.shinys.queries.repository.entity.Card;
import com.shinys.queries.repository.entity.Member;
import com.shinys.queries.service.CardQueryService;
import com.shinys.queries.service.MemberQueryOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 쿼리 인터페이스
 * @author shinys
 */
@RestController
@RequestMapping("/api/query")
public class RetrieveController {
    private final static Logger logger = LogManager.getLogger(RetrieveController.class);

    private static final AtomicLong sequence = new AtomicLong(1L);

    private WebClient webClient;

    private CardQueryService cardQueryService;

    private MemberQueryOperation memberQueryOperation;

    @Autowired
    public RetrieveController(
            CardQueryService cardQueryService,
            MemberQueryOperation memberQueryOperation,
            WebClient webClient
    ){
        this.cardQueryService = cardQueryService;
        this.memberQueryOperation = memberQueryOperation;
        this.webClient = webClient;
    }


    @GetMapping("/external-cards")
    public Mono<String> externalCards(
    ){
        return webClient.get()
                .uri("https://api.onstove.com/community/v1.0/main?access_token=&game_nos=8,141,26&size=15&display_field=game_content&timestemp=1561199626699")
                .retrieve()
                .bodyToMono(String.class);
    }

    @GetMapping("/cards")
    public Mono<ResponseDto<List<Card>>> cards(
    ){
        return  cardQueryService.list()
                .collectList() // Flux<Card>를 Mono<List<Card>>로 collecting
                .flatMap(cards -> Mono.just( new ResponseDto<>("000","OK", cards,null) ))
                .onErrorResume(Exception.class, e -> Mono.just( new ResponseDto<>("500","ERROR",null, e) ));
    }

    @GetMapping("/members")
    public Flux<Member> member(
    ){
        return memberQueryOperation.findMembers();
    }

}
