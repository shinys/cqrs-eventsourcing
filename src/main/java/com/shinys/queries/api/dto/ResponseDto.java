package com.shinys.queries.api.dto;

import lombok.*;

/**
 *
 * @author shinys
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ResponseDto<T> {
    String code;
    String message;
    T value;
    Throwable trace;
}
