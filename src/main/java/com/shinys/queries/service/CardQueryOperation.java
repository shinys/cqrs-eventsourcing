package com.shinys.queries.service;

import com.shinys.queries.repository.CardRepository;
import com.shinys.queries.repository.MemberRepository;
import com.shinys.queries.repository.entity.Card;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 *
 * @author shinys
 */
@Service
public class CardQueryOperation {
    private static final Logger logger = LogManager.getLogger(CardQueryOperation.class);

    private CardRepository cardRepository;

    @Autowired
    public CardQueryOperation(
            @Qualifier("cardRepository") CardRepository cardRepository
    ){
        this.cardRepository = cardRepository;
    }

    public Mono<Card> findCard(final String cardNo){
        logger.info("cardNo:{}",()->cardNo);
        return cardRepository.findFirstByCardNo(cardNo);
    }

    public Flux<Card> findCards(final List<String> cardNos){
        logger.info("cardNos:{}",()->cardNos);
        return cardRepository.findByCardNoIn(cardNos, Sort.by(Sort.Direction.DESC,"cardNo"));
    }

    public Flux<Card> findCards(){
        return cardRepository.findAll(Sort.by(Sort.Direction.DESC,"cardNo"));
    }

    public Mono<Card> saveCard(final Card card){
        logger.info("card:{}",()->card);
        return cardRepository.save(card);
    }

}
