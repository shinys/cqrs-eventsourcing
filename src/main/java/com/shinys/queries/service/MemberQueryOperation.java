package com.shinys.queries.service;

import com.shinys.queries.repository.MemberRepository;
import com.shinys.queries.repository.entity.Member;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 *
 * @author shinys
 */
@Service
public class MemberQueryOperation {
    private static final Logger logger = LogManager.getLogger(MemberQueryOperation.class);

    private MemberRepository memberRepository;

    @Autowired
    public MemberQueryOperation(
            @Qualifier("memberRepository") MemberRepository memberRepository
    ){
        this.memberRepository = memberRepository;
    }

    public Flux<Member> findMembers(List<Long> memberNos){
        return memberRepository.findByMemberNoIn(memberNos);
    }

    public Flux<Member> findMembers(){
        return memberRepository.findAll();
    }

    public Mono<Member> findMember(Long memberNo){
        logger.info("멤버검색 memberNo:{}",()->memberNo);
        return memberRepository.findByMemberNo(memberNo);
    }

    public Mono<Member> save(Member member){
        return memberRepository.save(member);
    }
}
