package com.shinys.queries.service;

import com.shinys.queries.repository.entity.Card;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author shinys
 */
@Service
public class CardQueryService {
    private static final Logger logger = LogManager.getLogger(CardQueryService.class);

    private CardQueryOperation cardQueryOperation;

    private MemberQueryOperation memberQueryOperation;

    @Autowired
    public CardQueryService(
            CardQueryOperation cardQueryOperation,
            MemberQueryOperation memberQueryOperation
    ){
        this.cardQueryOperation = cardQueryOperation;
        this.memberQueryOperation = memberQueryOperation;
    }

    public Mono<Card> save(Card card){
        return cardQueryOperation.saveCard(card);
    }

    public Flux<Card> list(){
        return cardQueryOperation.findCards()
                .map(card-> {
                    logger.info("멤버정보취득 cardNo:{},memberNo:{}",card::getCardNo,card::getMemberNo);
                    memberQueryOperation
                            .findMember(card.getMemberNo())
                            .subscribe(member->card.setMember(member));
                    return card;
                });
    }
}
