package com.shinys.queries.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author shinys
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "card")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Card  implements Serializable {
    private static final long serialVersionUID = 6512332286534302515L;

    @Id
    @JsonIgnore
    private ObjectId id;

    private String cardNo;

    private Long communityNo;

    @CreatedBy
    private long memberNo;

    @Transient
    private Member member;

    protected String title;

    protected String content;

    @CreatedDate
    protected LocalDateTime regDt;

    @LastModifiedDate
    protected LocalDateTime updDt;

}
