package com.shinys.queries.repository;

import com.shinys.queries.repository.entity.Member;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 *
 * @author shinys
 */
@Repository("memberRepository")
public interface MemberRepository extends ReactiveMongoRepository<Member, Long> {

    Mono<Member> findByMemberNo(Long memberNo);

    Flux<Member> findByMemberNoIn(List<Long> memberNo);
}
