package com.shinys.queries.repository;

import com.shinys.queries.repository.entity.Card;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 *
 * @author shinys
 */
@Repository("cardRepository")
public interface CardRepository extends ReactiveMongoRepository<Card, Long> {

    Flux<Card> findByCardNoIn(List<String> cardNos, Sort sort);

    Mono<Card> findFirstByCardNo(String cardNo);
}
