package com.shinys.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shinys.queries.repository.entity.Card;
import com.shinys.queries.repository.entity.Member;
import com.shinys.queries.service.CardQueryService;
import com.shinys.queries.service.MemberQueryOperation;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Event Handler
 * 이벤트버스를 통해 수신한 이벤트를 토대로 읽기 데이터베이스 갱신 및 부가 처리 진행.
 *
 * @author shinys
 */
@Service
public class ReadModelUpdater {
    private static final Logger logger = LogManager.getLogger(ReadModelUpdater.class);

    private ObjectMapper objectMapper;

    private CardQueryService cardQueryService;

    private MemberQueryOperation memberQueryOperation;

    @Autowired
    public ReadModelUpdater(
            CardQueryService cardQueryService,
            MemberQueryOperation memberQueryOperation,
            ObjectMapper objectMapper
    ){
        this.cardQueryService = cardQueryService;
        this.memberQueryOperation = memberQueryOperation;
        this.objectMapper = objectMapper;
    }

    @KafkaListener(topics = "card.create", groupId="consumer.group.1")
    public void onCardCreateEvent(ConsumerRecord<?, ?> consumerRecord) throws IOException {
        logger.info( "received payload='{}'", consumerRecord::toString );
        //이벤트 -> 엔티티 변환
        Card event = objectMapper.readerFor(Card.class).readValue(String.valueOf(consumerRecord.value()));
        event.setRegDt(LocalDateTime.now());

        //Read Database 갱신
        cardQueryService
                .save(event)
                .subscribe(card -> {
                    //알림, 로깅 등 엔티티 변화에 따른 후속 처리

                });
    }

    @KafkaListener(topics = "card.update", groupId="consumer.group.1")
    public void onCardUpdateEvent(ConsumerRecord<?, ?> consumerRecord) throws IOException {
        logger.info( "received payload='{}'", consumerRecord::toString );
        //이벤트 -> 엔티티 변환
        Card event = objectMapper.readerFor(Card.class).readValue(String.valueOf(consumerRecord.value()));
        event.setUpdDt(LocalDateTime.now());

        //Read Database 갱신
        cardQueryService
                .save(event)
                .subscribe(card -> {
                    //알림, 로깅 등 엔티티 변화에 따른 후속 처리

                });
    }

    @KafkaListener(topics = "member.update", groupId="consumer.group.1")
    public void onMemberUpdateEvent(ConsumerRecord<?, ?> consumerRecord) throws IOException {
        logger.info( "received payload='{}'", consumerRecord::toString );
        //이벤트 -> 엔티티 변환
        Member event = objectMapper.readerFor(Member.class).readValue(String.valueOf(consumerRecord.value()));
        event.setRegDt(LocalDateTime.now());

        //Read Database 갱신
        memberQueryOperation
                .save(event)
                .subscribe();
    }

}
