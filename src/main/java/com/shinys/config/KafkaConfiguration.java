package com.shinys.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.kafka.config.*;
import org.springframework.kafka.core.*;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;


/**
 *
 * @author shinys
 */
@Configuration
@EmbeddedKafka(
        partitions = 1, controlledShutdown = false,
        brokerProperties = {"listeners=PLAINTEXT://localhost:9092", "port=9092"},
        ports = {9092},
        topics = {"card.update", "card.create"}
)
@PropertySource("classpath:application.properties")
public class KafkaConfiguration {
    Logger logger = LogManager.getLogger(KafkaConfiguration.class);

    private static final String _EVENT_SOURCING = "event.bus";


    @Autowired
    KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

    static EmbeddedKafkaBroker _embeddedKafkaBroker;

    static{
        System.setProperty(EmbeddedKafkaBroker.BROKER_LIST_PROPERTY, "spring.kafka.bootstrap-servers");
        EmbeddedKafkaBroker embeddedKafkaBroker = new EmbeddedKafkaBroker(1, true, _EVENT_SOURCING);
        embeddedKafkaBroker.kafkaPorts(9092);
        embeddedKafkaBroker.afterPropertiesSet();
        _embeddedKafkaBroker = embeddedKafkaBroker;
    }


    ///////////////////
    // consumer
    ///////////////////
    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        EmbeddedKafkaBroker embeddedKafkaBroker = new EmbeddedKafkaBroker(1, true, _EVENT_SOURCING);
        embeddedKafkaBroker.kafkaPorts(9092);
        return new DefaultKafkaConsumerFactory<>(KafkaTestUtils.consumerProps("consumer.group.1","true",_embeddedKafkaBroker));
    }

    @Bean
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.getContainerProperties().setPollTimeout(3000);
        return factory;
    }


    ///////////////////
    // producer
    ////////////////////
    @Bean
    public ProducerFactory<String, String> producerFactory() {
        EmbeddedKafkaBroker embeddedKafkaBroker = new EmbeddedKafkaBroker(1, true, _EVENT_SOURCING);
        embeddedKafkaBroker.kafkaPorts(9092);
        return new DefaultKafkaProducerFactory<>(KafkaTestUtils.producerProps(_embeddedKafkaBroker));
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        KafkaTemplate<String, String> kafkaTemplate = new KafkaTemplate<>(producerFactory());
        kafkaTemplate.setDefaultTopic(_EVENT_SOURCING);
        return kafkaTemplate;
    }
}
