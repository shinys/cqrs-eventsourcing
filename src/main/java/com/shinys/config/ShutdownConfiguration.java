package com.shinys.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author shinys
 */
@Configuration
public class ShutdownConfiguration {

    @Bean
    public TerminateBean getTerminateBean() {
        return new TerminateBean();
    }
}
