package com.shinys.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author shinys
 */
@Configuration
public class ContextConfig {

    @Bean
    public ObjectMapper objectMapper(){

        return new ObjectMapper()
                //.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                .enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS
                        , DeserializationFeature.USE_LONG_FOR_INTS
                        , DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT
                        , DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT
                        , DeserializationFeature.READ_ENUMS_USING_TO_STRING
                        , DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                .enable(SerializationFeature.WRITE_NULL_MAP_VALUES
                        , SerializationFeature.WRITE_ENUMS_USING_TO_STRING
                        , SerializationFeature.INDENT_OUTPUT)
                .enable(JsonGenerator.Feature.IGNORE_UNKNOWN)
                .enable(MapperFeature.DEFAULT_VIEW_INCLUSION)
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule()); // new module, NOT JSR310Module

    }
}
