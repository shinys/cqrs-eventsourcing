package com.shinys.config;

import de.flapdoodle.embed.mongo.MongodProcess;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PreDestroy;

/**
 * @author shinys
 */
public class TerminateBean {
    @Autowired
    MongodProcess mongod;

//    @Autowired
//    MongodExecutable mongodExecutable;

    @PreDestroy
    public void onDestroy(){
        System.out.println( "Spring Container is destroyed!" );
        System.out.println("MongodProcess closing.. ");
        mongod.stopInternal();
//        mongodExecutable.stop();
    }
}
