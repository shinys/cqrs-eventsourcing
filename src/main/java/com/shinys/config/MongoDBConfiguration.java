package com.shinys.config;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import de.flapdoodle.embed.mongo.Command;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.*;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.config.IRuntimeConfig;
import de.flapdoodle.embed.process.extract.ITempNaming;
import de.flapdoodle.embed.process.runtime.Network;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import java.io.IOException;

/**
 * @author shinys
 */
@Configuration
public class MongoDBConfiguration extends AbstractReactiveMongoConfiguration {
    private static final Logger logger = LogManager.getLogger(MongoDBConfiguration.class);

    private static final String monogohost = "127.0.0.1";
    private static final int mongoPort = 21721;

    /**
     * 임베디드 몽고DB
     * https://github.com/flapdoodle-oss/de.flapdoodle.embed.mongo#custom-database-directory
     * @return
     * @throws IOException
     */
    @Bean(destroyMethod = "stop")
    public MongodProcess mongod() throws IOException {
        IRuntimeConfig runtimeConfig = new RuntimeConfigBuilder()
                .defaults(Command.MongoD)
                .artifactStore(new ExtractedArtifactStoreBuilder()
                        .defaults(Command.MongoD)
                        .executableNaming(new ITempNaming() {
                            @Override
                            public String nameFor(String prefix, String postfix) {
                                return prefix+"embeded"+postfix;
                            }
                        })
                )
                .daemonProcess(true)
                .build();
        MongodStarter starter = MongodStarter.getInstance(runtimeConfig);

        IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
                .net(new Net(monogohost, mongoPort, Network.localhostIsIPv6()))
                .timeout(new Timeout(30000))
                .stopTimeoutInMillis(6000)
                .build();
        MongodExecutable mongodExecutable = starter.prepare(mongodConfig);
        return mongodExecutable.start();

    }

    @Primary
    @Bean
    public ReactiveMongoTemplate reactiveMongoTemplate(){
        return new ReactiveMongoTemplate(reactiveMongoClient(),getDatabaseName());
    }

    @Override
    protected String getDatabaseName() {
        return "cqrs";
    }

    @Override
    public MongoClient reactiveMongoClient() {
        try {
            mongod();
        }catch(Exception e){
            e.printStackTrace();
        }
        return MongoClients.create("mongodb://"+monogohost+":"+mongoPort+"/"+getDatabaseName());
    }
}
