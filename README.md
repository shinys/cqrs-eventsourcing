# CQRS & Event Sourcing PoC Application  
> 간단한 웹 Rest API 기반 CRUD 작업을 통해 CQRS, 이벤트소싱 개념 이해와 서비스 적용 가능성 검토에 목적을 둠. 

### CQRS & Event Sourcing Reference Documentations  

MSDN : https://msdn.microsoft.com/ko-kr/magazine/mt147237.aspx  
CQRS (마틴파울러) : https://martinfowler.com/bliki/CQRS.html  
Event Sourcing (마틴파울러) : https://martinfowler.com/eaaDev/EventSourcing.html  


### application 
> 본 어플리케이션은 다음 수준의 CQRS 를 구현하고 있음.  

![Alt 구조 - inspired by nexocode.com](./document/cqrs-eventsourcing1.png)  
Inspired by nexocode.com



### Prerequisite
> 다음 기술에 대한 배경지식이 있으면 코드 보기가 수월하다.   

* Java 11  
* Gradle 5.x  
* Spring boot 2  
* Reactive 프로그래밍 ( Mono, Flux )  
* MongoDB, Spring-Data  
* Kafka  


### Build
```
$ gadle clean build --debug 
```


### Run Application  
> 본 어플리케이션은 몽고DB와 Kafka를 포함하여 구동에 필요한 모든것을 단일 jar에 포함하고 있음. 
```
$ java -jar ./build/libs/cqrs-eventsourcing-0.0.1-SNAPSHOT.jar
```
or 
```
$ gradle bootRun
```


### Execution  
>  commands 측 인터페이스는 /api/command 로, queries 측 인터페이스는 /api/query 로 시작한다.  

#### 멤버 등록  
```
curl -X POST \
  http://localhost:8000/api/command/member \
  -H 'Accept: application/json' \
  -H 'Accept-Charset: utf-8' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'accept-encoding: gzip, deflate' \
  -d '{
	"nick":"nick-1000"
}'
```

#### 멤버 확인  
```
curl -X GET \
  http://localhost:8000/api/query/members \
  -H 'Accept: application/json' \
  -H 'Accept-Charset: utf-8' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'accept-encoding: gzip, deflate' 
```

#### 카드 등록  
```
curl -X POST \
  http://localhost:8000/api/command/card \
  -H 'Accept: application/json' \
  -H 'Accept-Charset: utf-8' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'accept-encoding: gzip, deflate' \
  -d '{
	"community_no":1,
	"member_no":1,
	"title":"안녕하세요",
	"content":"테스트"
}'
```

#### 카드 확인  
```
curl -X GET \
  http://localhost:8000/api/query/cards \
  -H 'Accept: application/json' \
  -H 'Accept-Charset: utf-8' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'accept-encoding: gzip, deflate' 
```



### packages

```
com.shinys -|
            |- config                  어플리케이션 구동 configurations.
            |- commands -|             CQRS의 Commands 측.
                         |- api
                         |- domain
                         |- service    Command Handler.
                         |- repository
            |- event                   Event Handler.
            |- queries -|              CQRS의 Queries 측.
                        |- api
                        |- repository
                        |- service
            |- Application.java        어플리케이션 entry point.        
```



### 프로덕션 레벨의 CQRS 서비스 구축  
* springboot 
* axon framework 
* MQ or Kafka 


### 여전히 궁금한 것.

 조회와 명령이 동시에 필요한 경우는 어떻게 처리하지?  
 예를들어 조회하고 조회수 증가와 같은 거 말이지...  
 아키텍처를 따르면 조회에서 명령쪽으로 카은트 증가 명령을 수행해야할것 같은데..   
 결국 한바퀴돌아 metalized viwe 만드는곳으로 이벤트가 흘러오도록 한다?  
 명령 서비스랑 조회 서비스 사이에는 gRPC 같은 걸 끼얹으면 되나? 
 <-- 구현 복잡도가 늘어나는거 아님?  







### 개념잡기 좋은 글

https://www.infoq.com/articles/microservices-aggregates-events-cqrs-part-1-richardson/

https://www.infoq.com/articles/microservices-aggregates-events-cqrs-part-2-richardson/

### 이 어플리케이션을 작성하는데 영감을 준 articles

https://www.kennybastani.com/2017/01/building-event-driven-microservices.html

https://javacan.tistory.com/entry/dev-story-abount-ES-CQRS-springboot-angujarjs  

https://blog.aliencube.org/ko/2015/11/12/building-applications-on-cloud-with-event-sourcing-pattern-and-cqrs-pattern/  





